<?php
/**
 * Hook to add a HTML template to use in Javascript
 *
 * @return array
 */
function hook_javascript_templates_add() {
  return array(
    "my_new_template" => theme("my_tpl", array(
      "my_var" => "my_value"
    )),
    "my_second_tpl" => file_get_contents("some_file.tpl.html")
  );
}

/**
 * Hook to alter the available templates to use in Javascript
 *
 * @param $templates
 */
function hook_javascript_templates_alter(&$templates) {
  unset($templates['my_new_template']);
}